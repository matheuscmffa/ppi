<html>

    <head>
        <?php
        require_once 'head.php';
        ?>
    </head>
    <body>
        <?php require_once 'menu_superior.php';
        ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 p-2">
                    <h1>Funcionalidades</h1>
                    <p>Nesta página você poderá gerenciar sua loja facilmente.</p>
                    <p>Clique em Inserir no menu superior para acessar o formulário e incluir um novo produto no banco de dados.</p>
                    <p>Clique em Listar para visualizar todos os produtos inseridos no banco. É aqui onde você pode, também, editá-los ou excluí-los.</p>
                </div>
                <div class="col-lg-6 p-2 text-center">
                    <img class="img-fluid" src="img/edit-png-icon-blue-pencil-18.png">
                </div>
            </div>
        </div>

        <footer>
            <?php
            require_once 'footer.php';
            ?>
        </footer>
    </body>

</html>
