<html>
    <head>
        <?php require_once 'head.php'; ?>
    </head>
    <body>

        <nav class="navbar navbar-expand-lg sticky-top navbar-light" style="background-color: #86ba90;">
            <a class="navbar-brand" href="#">Gerenciamento de Produtos</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
                <div class="navbar-nav">

                    <a class="nav-item nav-link" href="index.php">Home</a>
                    <a class="nav-item nav-link" href="form_produto.php">Inserir</a>
                    <a class="nav-item nav-link" href="produtos.php">Listar</a>

                </div>
            </div>
        </nav>
    </body>
</html>