<html>
    <head>
        <?php
        require_once 'head.php';
        ?>
    </head>

    <body>
        <?php require_once 'menu_superior.php'; ?>

        <div class="container">
            <div class="row justify-content-center"><h1>Formulário de inserção</h1></div>
        </div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col col-sm-12 col-md-6">
                    <hr>
                    <form action="Controller/ProdutoController.php" method="POST" class="form" enctype="multipart/form-data">
                        <input type="hidden" name="acao" value="salvar">
                        <div class="form-group">
                            <p>Nome:</p>
                            <input type="text" class="form-control" name="nome" placeholder="Nome do produto">
                        </div>
                        <div class="form-group">
                            <p>Preço:</p>
                            <input type="text" class="form-control" name="preco" placeholder="Preço em dólar">
                        </div>

                        <div class="form-group">
                            <p>Quantidade:</p>
                            <input type="number" class="form-control" name="quantidade" min="1">
                        </div>
                        <div class="form-group">
                            <p>Categoria:</p>
                            <select name="categoria" id="categoria" class="form-control">
                                <option value="">Selecione:</option>
                                <option value="Filmes">Filmes</option>
                                <option value="Livros">Livros</option>
                                <option value="Sapatos">Sapatos</option>
                            </select>

                        </div>
                        <div>
                            <p>Imagem:</p>
                            <input type="file" class="form-control" name="imagem" accept="*/image">
                        </div>
                        <hr>
                        <div class="botaoCadastrar">
                            <input type="submit" value="Cadastrar">
                        </div>
                    </form>

                </div>

            </div>
        </div>
        <footer>
            <?php require_once 'footer.php'; ?>
        </footer>
    </body>

</html>

