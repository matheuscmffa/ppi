<?php
ini_set('display_errors', 1);
require_once 'Dao/ProdutoDao.php';
$produtoDao = new ProdutoDao();

$produtos = $produtoDao->listarProdutos();

?>

<!DOCTYPE html>
<html>
    <?php
    require_once 'head.php';
    ?>

    <body> 
        <?php
        require_once 'menu_superior.php';
        ?>

        <div class="container">
            <div class="row justify-content-center"><h1>Listagem de produtos</h1></div></div>
        <div class="row text-center">
            <div class="col-md-6">
            <form action="busca.php">
                <label for="search">Busca: </label>
                <input type="text" name="search" id="search" placeholder="Nome do produto...">
                </div>

            <div class="col-md-6">
                <label for="categoria">Categoria: </label>
                <select name="categoria">
                    <option value="">TODAS</option>
                    <option value="FILMES">Filmes</option>
                    <option value="LIVROS">Livros</option>
                    <option value="SAPATOS">Sapatos</option>
                    
                </select>


                <input type="submit" value="Buscar">
            </form>
            </div>

        </div>
        <div class="container">
            <div class="row text-center justify-content-center">
                <?php foreach ($produtos as $produto) { ?>
                    <div class="col-sm-6 col-md-3 p-2">
                        <img src="uploads/<?php echo "$produto->imagem" ?>" width="150" height="150"><br>
                        ID: <?= $produto->id ?><br>
                        Nome: <?= $produto->nome ?><br>
                        Preço: <?= $produto->preco ?><br>
                        Quantidade: <?= $produto->quantidade ?><br>
                        Categoria: <?= $produto->categoria ?><br>

                        <?php echo "<a href=\"editar.php?id=$produto->id\">Editar</a> |<a href=\"delete.php?id=$produto->id\" onClick=\"return confirm('Tem certeza de que deseja excluir?')\">Excluir</a></td>"; ?>


                    </div>
                <?php } ?>
            </div>
        </div>
        <footer>
            <?php
            require_once 'footer.php';
            ?>
        </footer>
    </body>
</html>