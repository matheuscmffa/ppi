<?php
require_once $_SERVER ['DOCUMENT_ROOT'] . "/Util/Conexao.php";


if (isset($_POST['update'])) {
    $id = $_POST['id'];

    $nome = strtoupper($_POST['nome']);
    $preco = $_POST['preco'];
    $quantidade = $_POST['quantidade'];
    $categoria = strtoupper($_POST['categoria']);


    $images = $_FILES['imagem']['name'];
    $tmp_dir = $_FILES['imagem']['tmp_name'];
    $imageSize = $_FILES['imagem']['size'];

    $upload_dir = '../uploads/';

    $imgExt = strtolower(pathinfo($images, PATHINFO_EXTENSION));
    $valid_extensions = array('jpeg', 'jpg', 'png');
    $produto['imagem'] = rand(1000, 1000000) . "." . $imgExt;
    unlink($upload_dir . $edit_row['imagem']);
    move_uploaded_file($tmp_dir, $upload_dir . $produto['imagem']);


    $sql = "UPDATE produtos SET nome=:nome, preco=:preco, quantidade=:quantidade, categoria=:categoria, imagem=:imagem WHERE id=:id";
    $p_sql = Conexao::getInstancia()->prepare($sql);

    $p_sql->bindparam(':id', $id);
    $p_sql->bindparam(':nome', strtoupper($nome));
    $p_sql->bindparam(':preco', $preco);
    $p_sql->bindparam(':quantidade', $quantidade);
    $p_sql->bindparam(':categoria', strtoupper($categoria));
    $p_sql->bindparam(':imagem', $imagem);

    $p_sql->execute();


    header("Location: produtos.php");
}
?>
<?php
$id = $_GET['id'];

$sql = "SELECT * FROM produtos WHERE id=:id";
$p_sql = Conexao::getInstancia()->prepare($sql);
$p_sql->execute(array(':id' => $id));

while ($row = $p_sql->fetch(PDO::FETCH_ASSOC)) {
    $nome = $row['nome'];
    $preco = $row['preco'];
    $quantidade = $row['quantidade'];
    $categoria = $row['categoria'];
    $imagem = $row['imagem'];
}
?>
<html>
    <head>	
<?php require_once 'head.php'; ?>

        <title>Edit Data</title>
    </head>

    <body>

<?php require_once 'menu_superior.php'; ?>
        <div class="container">
            <div class="row justify-content-center"><h1>Formulário de edição</h1>
            </div>
            <div class="row justify-content-center"><h2>Altere os campos que desejar</h2>
            </div>

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col col-sm-12 col-md-6">
                        <hr>
                        <form name="form1" method="post" action="editar.php" enctype="multipart/form-data">

                            <div class="form-group">
                                <p>Nome:</p>
                                <input type="text" class="form-control" name="nome" value="<?php echo $nome; ?>">
                            </div>
                            <div class="form-group">
                                <p>Preço:</p>
                                <input type="text" class="form-control" name="preco" value="<?php echo $preco; ?>">
                            </div>

                            <div class="form-group">
                                <p>Quantidade:</p>
                                <input type="number" class="form-control" name="quantidade" min="1" value="<?php echo $quantidade; ?>">
                            </div>

                            <div class="form-group">
                                <p>Categoria:</p>
                                <select name="categoria" class="form-control">
                                    <option value="">Selecione:</option>
                                    <option value="FILMES">Filmes</option>
                                    <option value="LIVROS">Livros</option>
                                    <option value="SAPATOS">Sapatos</option>
                                </select>

                            </div>

                            

                            <div>
                                <p>Imagem:</p>
                                <input type="file" class="form-control" name="imagem" accept="*/image">
                            </div>
                            <input type="hidden" name="id" value=<?php echo $_GET['id']; ?>>
                            <hr>
                            <div class="botaoCadastrar">
                                <input type="submit" name="update" value="Cadastrar">
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>

        <footer>
<?php require_once 'footer.php'; ?>
        </footer>
    </body>
</html>

