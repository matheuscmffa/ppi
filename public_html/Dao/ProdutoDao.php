<?php

require_once $_SERVER ['DOCUMENT_ROOT'] . "/Util/Conexao.php";

class ProdutoDao {

    public function __construct() {
        
    }

    public function salvar($produto) {
        try {
            $images = $_FILES['imagem']['name'];
            $tmp_dir = $_FILES['imagem']['tmp_name'];
            $imageSize = $_FILES['imagem']['size'];

            $upload_dir = '../uploads/';

            $imgExt = strtolower(pathinfo($images, PATHINFO_EXTENSION));
            $valid_extensions = array('jpeg', 'jpg', 'png');
            $produto['imagem'] = rand(1000, 1000000) . "." . $imgExt;
            move_uploaded_file($tmp_dir, $upload_dir . $produto['imagem']);

            $sql = 'insert into produtos (nome, preco, quantidade, categoria, imagem)
         values (:nome, :preco, :quantidade, :categoria, :imagem)';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':nome', $produto['nome']);
            $p_sql->bindValue(':preco', $produto['preco']);
            $p_sql->bindValue(':quantidade', $produto['quantidade']);
            $p_sql->bindValue(':categoria', $produto['categoria']);
            $p_sql->bindValue(':imagem', $produto['imagem']);
            $p_sql->execute();
            header("Location:../produtos.php");
            return;
        } catch (Exception $e) {
            print_r($e);
        }
    }

    public function listarProdutos() {
        try {
            $sql = 'select * from produtos';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            print_r($e);
        }
    }


    public function excluir($produto) {
        try {
            $sql = 'delete from produtos where id = :id';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->execute(array(':id' => $produto['id']));

            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            print_r($e);
        }
    }

}
